
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "ap-southeast-1"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}


resource "aws_subnet" "privateSubnetAZ1A" {
  vpc_id     = aws_default_vpc.default.id
  cidr_block = "172.32.1.0/24"
  availability_zone = "ap-southeast-1a"
  tags = {
    Name = "privateappServerSubnetAZ1A"
  }
}
resource "aws_subnet" "privateSubnetAZ1B" {
  vpc_id     = aws_default_vpc.default.id
  cidr_block = "172.32.2.0/24"
  availability_zone = "ap-southeast-1b"
  tags = {
    Name = "privateappServerSubnetAZ1B"
  }
}
resource "aws_subnet" "privateSubnetAZ1C" {
  vpc_id     = aws_default_vpc.default.id
  cidr_block = "172.32.3.0/24"
  availability_zone = "ap-southeast-1c"
  tags = {
    Name = "privateappServerSubnetAZ1C"
  }
}

resource "aws_iam_policy" "fullAccessToS3" {
    name = "fullAccessToS3"
    path = "/"
    description="Full Access Policy to S3"

    policy= jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "VisualEditor0",
                    "Effect": "Allow",
                    "Action": "s3:*",
                    "Resource": "*"
                }
            ]
        }

    )

}

resource "aws_iam_role" "govtechAssessment20221007S3FullAccessRole" {
    name = "govtechAssessment20221007S3FullAccessRole"
    assume_role_policy = jsonencode(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    Action  = "sts:AssumeRole"
                    Effect  = "Allow"
                    Sid     = ""
                    Principal   = {
                        Service = "ec2.amazonaws.com"
                    }
                }
            ]
        }
    )
}

resource "aws_iam_policy_attachment" "ec2_policy_role" {
    name = "ec2_s3_fullaccess"
    roles= [aws_iam_role.govtechAssessment20221007S3FullAccessRole.name]
    policy_arn= aws_iam_policy.fullAccessToS3.arn
}

resource "aws_iam_instance_profile" "ec2_profile" {
    name = "ec2_profile"
    role= aws_iam_role.govtechAssessment20221007S3FullAccessRole.name
}
/*Load Balancer*/
resource "aws_lb" "govtechAssessment20221007ALB" {
  name               = "govtechAssessment20221007ALB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_access_from_internet.id]
  subnets            = ["subnet-dd599e95","subnet-1ca4727a","subnet-dfdd5086"]
  enable_deletion_protection = false
  tags = {
    Environment = "DEV"
  }
}
/*Scalling*/
/*Security Group*/
resource "aws_security_group" "allow_access_from_ALB" {
  name        = "allow_access_from_ALB"
  description = "Allow all traffic inbound from ALB"

  ingress {
    description      = "ALB inbound Access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    security_groups      = [aws_security_group.allow_access_from_internet.id]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    security_groups  = [aws_security_group.allow_access_from_internet.id]
  }

  tags = {
    Name = "allow_access_from_ALB"
  }
}
/*Target Group*/
resource "aws_lb_target_group" "govtechAssessmentTargetGroup" {
  name        = "govtechAssessmentTargetGroup"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_default_vpc.default.id
}
# resource "aws_lb_target_group_attachment" "govtechAssessmentTargetGroupAtn" {
#   target_group_arn = aws_lb_target_group.govtechAssessmentTargetGroup.arn
#   port             = 80
# }

resource "aws_lb_listener" "govtechAssessment20221007ALBListener" {
  load_balancer_arn = aws_lb.govtechAssessment20221007ALB.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.govtechAssessmentTargetGroup.arn
  }
}

resource "aws_security_group" "allow_access_from_internet" {
  name        = "allow_access_from_internet"
  description = "Allow http and SSH inbound"

  ingress {
    description      = "HTTP inbound Access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
    ingress {
    description      = "SSH inbound Access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_access_from_internet"
  }
}
/*EC2*/

# resource "aws_launch_template" "govtechAssessmentAppServerLch" {
#   name   = "govtechAssessmentAppServerLch"
#   image_id      = "ami-05e690273122266ec"
#   instance_type = "t2.micro"
#   iam_instance_profile  {
#     name = aws_iam_instance_profile.ec2_profile.name

#   }
#   vpc_security_group_ids = [aws_security_group.allow_access_from_internet.id]
#   tags = {
#       Name = "GovTechAssessment20221007Instances"
#   }
#   user_data =base64encode( <<EOF
#     #!/bin/bash
#     sudo mkdir -p /var/www/test/html
#     aws s3api get-object --bucket govtech-assessment-20221007-sample-website --key index.html /var/www/test/html/index.html
#     sudo systemctl start nginx.service
#     EOF
#   )
# }
resource "aws_autoscaling_group" "govtechAssessmentAppServeASG" {
  name = "govtechAssessmentAppServeASG"
  desired_capacity   = 3
  max_size           = 3
  min_size           = 1
  default_cooldown   = 60
  availability_zones = ["ap-southeast-1a","ap-southeast-1b","ap-southeast-1c"]
  launch_configuration      = aws_launch_configuration.app_server.id
  # launch_template {
  #   id      = aws_launch_template.govtechAssessmentAppServerLch.id
  #   version = "$Latest"
  # }
  target_group_arns = [aws_lb_target_group.govtechAssessmentTargetGroup.arn]
  lifecycle {
    create_before_destroy = true
  }
 }


 resource "aws_launch_configuration" "app_server" {
  name_prefix          = "app_server"
  image_id             = "ami-03c18969e3c7a6232"
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  security_groups      = [aws_security_group.allow_access_from_ALB.name]
  user_data =<<-EOF
    #!/bin/bash
    sudo mkdir -p /var/www/test/html
    aws s3api get-object --bucket govtech-assessment-20221007-sample-website --key index.html /var/www/test/html/index.html
    sudo cp  /var/www/test/html/index.html  /usr/share/nginx/html/index.html
    sudo systemctl start nginx.service
    EOF
  lifecycle {
    create_before_destroy = true
  }
  
}
# resource "aws_instance" "app_server" {
#     ami           = "ami-03c18969e3c7a6232"
#     instance_type = "t2.micro"
#     iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
#     security_groups = [aws_security_group.allow_access_from_internet.name]
#     tags = {
#         Name = "GovTechAssessment20221007Instances"
#     }

#     user_data = <<EOF
#     #!/bin/bash
#     sudo systemctl start nginx.service
#     /usr/local/bin/aws s3api get-object --bucket govtech-assessment-20221007-sample-website --key index.html /tmp/index.html
#     EOF

# }
